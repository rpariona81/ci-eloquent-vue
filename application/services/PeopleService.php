<?php
namespace Src\Services;

defined('BASEPATH') or exit('No direct script access allowed');

use Src\Domains\PeopleDao;

class PeopleService
{

    public function getAll()
    {
        return PeopleDao::all();
    }

    public function getAleatorio()
    {
        $number = random_int(0, 100);
        return $number;
    }

    public function newPeople($request)
    {
        $newPeople = new PeopleDao();
        $newPeople->firstname = 'Domitilo';
        $newPeople->lastname = 'Saavedra';
        $newPeople->gender = 'boy';
        $newPeople->save();
    }

    public function deletePeople($request)
    {
        $findPeople = PeopleDao::findOrFail($request->id);        
        $findPeople->delete();
    }
}
