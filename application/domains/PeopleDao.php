<?php
namespace Src\Domains;

defined('BASEPATH') or exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;

class PeopleDao extends Eloquent
{
    protected $table = 't_people';
    protected $fillable = [
        'firstname', 'lastname', 'gender', 'birthday', 'email', 'contact', 'address'
    ];

    /*protected $casts = [
        'condicion' => 'boolean',
    ];*/

    /*public function categoria()
    {
        return $this->belongsTo('App\Dao\Categoria');
    }*/
}
