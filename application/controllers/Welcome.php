<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Src\Services\PeopleService;

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	protected $service;

    public function __construct()
    {
        parent::__construct();
        $this->service = new PeopleService;
    }
	public function index()
	{
		//$this->load->view('welcome_message');
		/*
		header('Content-Type: application/json');
		echo json_encode($this->service->getAll());*/
		$this->output->set_status_header(200)->set_content_type('application/json')->set_output(json_encode($this->service->getAll()));

	}
	public function azar()
	{
		echo '<html><body>Lucky number: '.$this->service->getAleatorio().'</body></html>';
		
	}

	public function crear()
	{
		$this->service->newPeople($this->input->post());
	}
}
